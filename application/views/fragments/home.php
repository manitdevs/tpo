<div class="container-fluid">
    <h4>About Manit</h4>
    <h5>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h5>

</div>
<div class="container-fluid">
    <h4>Director's Message</h4>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-center" >
        <img src="/assets/img/logo.png" class="img-responsive" alt="Logo" style="max-height:100px !important;">
    </div>
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 " >
        <h5>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h5>

    </div>
</div>
<br>
<div class="container-fluid">
    <div class="well">
        <div class="container-fluid">
            <div class="col-md-offset-3 col-xs-12 col-sm-12 col-md-6 col-lg-2 text-center" >
                <div class="well">For Companies</div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-2 text-center" >
                <div class="well">For Students</div>
            </div>
        </div>
    </div>
</div>
