<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/slick.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/slick-theme.css"/>
		<link rel="stylesheet" type="text/css" href="/assets/css/loader.css"/>
	</head>
	<body style="overflow-x:hidden;">
		<div class="container-fluid bg-primary" >
			<div class="container" >

					<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2" >
						<img src="/assets/img/logo.png" class="img-responsive" alt="Logo" style="max-height:80px !important;margin:5px 0px;">
					</div>
					<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 text-center" >
						<h3><strong>Training And Placement Cell</strong></h3>
						<h4>Maulana Azad National Institute of Technology, Bhopal </h4>
					</div>

			</div>
		</div>
		<div class="row">
			<div class="slider">
				<!-- <ul class="bxslider" autoplay='on'> -->
				  <div><img src="/assets/img/pic1.jpg" class="img-responsive"/></div>
				  <div><img src="/assets/img/pic2.jpg" class="img-responsive"/></div>
				  <div><img src="/assets/img/pic3.jpg" class="img-responsive"/></div>
				  <div><img src="/assets/img/pic4.jpg" class="img-responsive"/></div>
				<!-- </ul> -->
			</div>

		</div>
		<div class="container">
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<ul class="list-group">
					  <li class="list-group-item"><h4>NOTIFICATIONS</h4></li>
					  <li class="list-group-item">Notification 1</li>
					  <li class="list-group-item">Notification 2</li>
					  <li class="list-group-item">Notification 3</li>
					  <li class="list-group-item">Notification 4</li>
					  <li class="list-group-item">Notification 5</li>
		 		</ul>
		 		<ul class="list-group">
					  <li class="list-group-item"><h4>QUICK LINKS</h4></li>
					  <li class="list-group-item"><a href="javascript:void(0)" class="nav" data-page="home">Home</a></li>
					  <li class="list-group-item"><a href="javascript:void(0)" class="nav" data-page="placement">Placement</a></li>
					  <li class="list-group-item"><a href="javascript:void(0)" class="nav" data-page="why">Why @ NIT-B</a></li>
					  <li class="list-group-item"><a href="javascript:void(0)" class="nav" data-page="departments">Departments</a></li>
					  <li class="list-group-item"><a href="javascript:void(0)" class="nav" data-page="team">Team</a></li>
					  <li class="list-group-item"><a href="javascript:void(0)" class="nav" data-page="downloads">Downloads</a></li>
					  <li class="list-group-item"><a href="javascript:void(0)" class="nav" data-page="contactus">Contact Us</a></li>

		 		</ul>
			</div>
			<div class="col-md-10 col-lg-10">
					<div id="page">



						<div class="container-fluid">
							<h4>About Manit</h4>
							<h5>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h5>

						</div>
						<div class="container-fluid">
							<h4>Director's Message</h4>
							<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-center" >
								<img src="/assets/img/logo.png" class="img-responsive" alt="Logo" style="max-height:100px !important;">
							</div>
							<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 " >
								<h5>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
								cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
								proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h5>

							</div>
						</div>
						<br>
						<div class="container-fluid">
							<div class="well">
								<div class="container-fluid">
									<div class="col-md-offset-3 col-xs-12 col-sm-12 col-md-6 col-lg-2 text-center" >
										<div class="well">For Companies</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-2 text-center" >
										<div class="well">For Students</div>
									</div>
								</div>
							</div>
						</div>



					</div>
			</div>
		</div>

		<div class="container">
			<div class="well">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
								<img src="/assets/img/logo.png" class="img-responsive" alt="Logo" style="max-height:100px !important;">
							</div>
							<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
								<h4>MANIT<br>BHOPAL</h4>
							</div>
						</div>
						<div class="row">
							Copyright &copy; Training and Placements Office, MANIT BHOPAL
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<h4>Reach Us</h4>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<img src="/assets/img/logo.png" class="img-responsive" alt="Logo" style="max-height:100px !important;">
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<h4>Links</h4>
						MANIT<br>
						ACADEMICS<br>
						PROFESSORS
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript" src="/assets/js/jquery.min.js"></script>
		<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/assets/js/slick.min.js"></script>
		<script type="text/javascript" src="/assets/js/jquery.blockUI.min.js"></script>
		<script type="text/javascript">
			$.blockUI.defaults = 	{
									    message:  '<div class="loader"></div>',
										css: {
										        padding:        0,
										        margin:         0,
										        width:          '30%',
										        top:            '30%',
										        left:           '35%',
										        textAlign:      'center',
										        color:          '#fff',
										        border:         '0',
										        backgroundColor:'transparent',
										        cursor:         'wait'
										    },
										baseZ: 9999999,
										overlayCSS:  {
														display:		'block',
												        backgroundColor: '#000',
												        opacity:         0.4,
												        cursor:          'wait'
												    },
										fadeIn:  200,
										fadeOut:  400
									};
			$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
			$(document).ready(function(){
				$('.slider').slick({
					accessibility: false,
					autoplay: true,
					autoplaySpeed: 3000,
					fade: true,
					pauseOnHover: false
				});

			});
			$(".nav").click(function(){
				page = $(this).data("page");
				 $.ajax({
					url: '/page',
					type: 'POST',
					dataType: 'html',
					data: 'page='+page,
					success: function (response) {
						var data = JSON.parse(response);
						$("div#page").html("");
						$("div#page").html(data.html);
						if (page == "home") {
							window.history.pushState('', '', '/');
						} else {
							window.history.pushState('', '', '/'+page);
						}

					}
				});

			});
		</script>

	</body>
</html>
