<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
    }

    function index()
    {
        $this->load->view('home');
    }
    function placement()
    {
        $this->load->view('placement');
    }
    function why()
    {
        $this->load->view('why');
    }
    function departments()
    {
        $this->load->view('departments');
    }
    function team()
    {
        $this->load->view('team');
    }
    function downloads()
    {
        $this->load->view('downloads');
    }
    function contactus()
    {
        $this->load->view('contactus');
    }
    function page(){
    	if ($this->input->is_ajax_request()) :
    		$page = $this->input->post('page');
            $string = read_file(APPPATH . '/views/fragments/' . $page . '.php');
            $pagedata = [
                            'html' => $string
            ];
            echo json_encode($pagedata);
    	endif ;
    }

}
